import React from 'react';
import { withProps } from 'recompose';
import { BackgroundImage, Box, Card, Small, Subhead } from 'rebass';

const Tile = ({ imgUrl, title, text }) => (
  <Box w={[1, 1 / 2, 1 / 4]}>
    <Card>
      <BackgroundImage src={imgUrl} />
      <Box p={2}>
        <Subhead>{title}</Subhead>
        <Small>{text}</Small>
      </Box>
    </Card>
  </Box>
);

export const Component = Tile;

const makeImgUrl = id => `https://d2snwnmzyr8jue.cloudfront.net/${id}_270.jpeg`;

export default withProps(({ artKey, title, artistName }) => ({
  title,
  key: artKey,
  text: artistName,
  imgUrl: makeImgUrl(artKey)
}))(Tile);
