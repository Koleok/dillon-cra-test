import React from 'react';
import { connect } from 'react-redux';
import { Box, Flex, Heading } from 'rebass';
import { arrayOf, bool, shape, string, number } from 'prop-types';

import Tile from './Tile';
import { requestBooks } from './redux/loader';

class App extends React.Component {
  static propTypes = {
    books: arrayOf(
      shape({
        titleId: number,
        title: string,
        kindId: number,
        kind: string,
        artistName: string,
        demo: bool,
        pa: bool,
        edited: bool,
        artKey: string,
        children: bool,
        fixedLayout: bool,
        readAlong: bool
      })
    )
  };

  static defaultProps = {
    books: []
  };

  componentDidMount() {
    this.props.dispatch(requestBooks());
  }

  render() {
    return (
      <div>
        <Box px={4} py={5} color="white" bg="blue">
          <Heading is="h1" fontSize={[4, 5, 6]}>
            Dillon Software Test
          </Heading>
        </Box>
        <Flex flexWrap="wrap" px={4} py={5}>
          {this.props.books.map(Tile)}
        </Flex>
      </div>
    );
  }
}

export const Component = App;

const mapStateToProps = ({ books }) => ({ books });

export default connect(mapStateToProps)(App);
