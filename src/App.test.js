import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { Component } from './App';
import 'jest-styled-components';

const App = (
  <Component
    dispatch={console.log}
    books={[
      {
        titleId: 12029295,
        title: 'Shirley Valentine',
        kindId: 7,
        kind: 'MOVIE',
        artistName: 'Pauline Collins',
        demo: false,
        pa: false,
        edited: false,
        artKey: 'par_32248001',
        children: false,
        fixedLayout: false,
        readAlong: false
      },
      {
        titleId: 11998107,
        title: 'Mad To Be Normal',
        kindId: 7,
        kind: 'MOVIE',
        artistName: 'David Tennant',
        demo: false,
        pa: false,
        edited: false,
        artKey: 'sgw_madtobenormal',
        children: false,
        fixedLayout: false,
        readAlong: false
      }
    ]}
  />
);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(App, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders to last known working state', () => {
  expect(renderer.create(App).toJSON()).toMatchSnapshot();
});
