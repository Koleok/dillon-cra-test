import { handleActions } from 'redux-actions';
import { BOOKS_FETCH_SUCCEEDED } from './';

export const defaultState = [];

export default handleActions(
  {
    [BOOKS_FETCH_SUCCEEDED]: (_, { payload }) => payload
  },
  []
);
