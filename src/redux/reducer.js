import { combineReducers } from 'redux';
import { default as books } from './loader/loader.reducer';

export default combineReducers({
  books
});
