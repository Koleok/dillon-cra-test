import { call, put, takeLatest } from 'redux-saga/effects';
import { createAction } from 'redux-actions';
import fetch from 'isomorphic-fetch';

const apiUrl =
  'https://hoopla-ws-dev.hoopladigital.com/kinds/7/titles/featured?offset=0&limit=51&kindId=7';

const apiCall = () =>
  fetch(apiUrl, { headers: { 'ws-api': '2.1' } }).then(res => res.json());

export const BOOKS_FETCH_SUCCEEDED = 'BOOKS_FETCH_SUCCEEDED';
export const succeeded = createAction(BOOKS_FETCH_SUCCEEDED);

export const BOOKS_FETCH_FAILED = 'BOOKS_FETCH_FAILED';
export const failed = createAction(BOOKS_FETCH_FAILED);

export const BOOKS_FETCH_REQUESTED = 'BOOKS_FETCH_REQUESTED';
export const requestBooks = createAction(BOOKS_FETCH_REQUESTED);

export function* fetchBooks() {
  try {
    const books = yield call(apiCall);
    yield put(succeeded(books));
  } catch (e) {
    yield put(failed(e.message));
  }
}

export default function* fetchBooksSaga() {
  yield takeLatest(BOOKS_FETCH_REQUESTED, fetchBooks);
}
