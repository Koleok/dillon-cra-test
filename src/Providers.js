import React from 'react';
import { Provider as RebassProvider } from 'rebass';
import { Provider as ReduxProvider } from 'react-redux';

import { store } from './redux';

const Providers = ({ children }) => (
  <ReduxProvider store={store}>
    <RebassProvider>{children}</RebassProvider>
  </ReduxProvider>
);

export default Providers;
